# !/bin/bash

SERVERIP={{ ip }}
SERVERPORT={{ port }}
SECRET={{ secret }}

# first query (update computer informations)
{% raw %}

# hostname
ho=$(hostname)

# GPU name
GPUname=$(lspci -nn | grep '\[03' | awk -F"]:" '{print $2}' | awk -F"(" '{print $1}')
GPUname=$(echo $GPUname | tr '/' '-')

# CPU cores
CPUcores=$(cat /proc/cpuinfo | grep -i "^processor" | wc -l)

# CPU model
CPUname=$(cat /proc/cpuinfo | grep -i "^model name" | awk -F": " '{print $2}' | head -1 | sed 's/ \+/ /g')

wget --spider "http://$SERVERIP:$SERVERPORT/data/${SECRET}/${ho}/${GPUname}/${CPUcores}/${CPUname}" 2>&1 | egrep -i "200"

echo "Computer's data successfully updated."
echo "Entering the loop in 5s."
sleep 5s

# infinite loop (probe function)
while :
do
	# ram % usage
	usedRam=$(free | grep Mem | awk '{print $3/$2 * 100.0}')

	# cpu % usage
	usedCPU=$(grep 'cpu ' /proc/stat | awk '{print ($2+$4)*100/($2+$4+$5)}')

	activeUsers=$(who | cut -d " " -f1 | paste -sd "," -)

	if [ -f /sys/class/thermal/thermal_zone0/temp ]; then
		tempSensors=$(cat /sys/class/thermal/thermal_zone*/{temp,type} | paste -sd "," -)
	else
		tempSensors="no information available"
	fi

	if [ -f /sys/class/thermal/cooling_device0/cur_state ]; then
		fanSensors=$(cat /sys/class/thermal/cooling_device*/{{cur,max}_state,type} | paste -sd "," -)
	else
		fanSensors="no information available"
	fi

	CPUfreq=$(cat /proc/cpuinfo | grep -i "^cpu MHz" | awk -F": " '{print $2}' | head -1)

	up=$(uptime -s)

	wget --spider "http://$SERVERIP:$SERVERPORT/data/${SECRET}/${usedRam}/${usedCPU}/${activeUsers}/${tempSensors}/${fanSensors}/${CPUfreq}/${up}" 2>&1 | egrep -i "200"

{% endraw %}
	sleep {{ frequency }}m
done