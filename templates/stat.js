var graph = [];
var node = [];

// thx https://stackoverflow.com/a/1091399/6813732
Highcharts.setOptions({
    time: {
        timezoneOffset: new Date().getTimezoneOffset()
    }
});


{% for log in logs %}
node = [Date.parse('{{ log[2] }}'),{{ log[4] }}];
graph.push(node);
{% endfor %}
Highcharts.chart('cpu', {
    chart: {
        type: 'line',
        zoomType: 'x'
    },
    title: {
        text: 'CPU usage over time'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            millisecond: '%H:%M:%S.%L',
            second: '%H:%M:%S',
            minute: '%H:%M',
            hour: '%H:%M',
            day: '%e. %b',
            week: '%e. %b',
            month: '%e. %b',
            year: '%Y'
        },
        title: {
            text: 'Date'
        }
    },
    yAxis: {
        title: {
            text: 'CPU usage (in %)'
        }
    },
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            }
        }
    },

    series: [{
        name: 'CPU usage',
        data: graph
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});

graph = []
{% for log in logs %}
node = [Date.parse('{{ log[2] }}'),{{ log[3] }}];
graph.push(node);
{% endfor %}
Highcharts.chart('ram', {
    chart: {
        type: 'line',
        zoomType: 'x'
    },
    title: {
        text: 'RAM usage over time'
    },
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
            millisecond: '%H:%M:%S.%L',
            second: '%H:%M:%S',
            minute: '%H:%M',
            hour: '%H:%M',
            day: '%e. %b',
            week: '%e. %b',
            month: '%e. %b',
            year: '%Y'
        },
        title: {
            text: 'Date'
        }
    },
    yAxis: {
        title: {
            text: 'RAM usage (in %)'
        }
    },
    legend: {
        enabled: false
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            }
        }
    },

    series: [{
        name: 'RAM usage',
        data: graph
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});