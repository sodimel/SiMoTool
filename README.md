SiMoTool
====
Simple Monitoring Tool
----


How to launch ?
====

linux
----
`export FLASK_APP=server.py && python3 -m flask run -h 0.0.0.0`

windows
----
`set FLASK_APP=server.py && py -m flask run -h 0.0.0.0`

CLI usage
----
`set FLASK_APP=server.py && py -m flask infos`

And tadaaah, you have a nice server that collect a lots of data from your computers.