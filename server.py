# !/usr/bin/python3
# -*- coding: utf-8 -*-

# linux
# export FLASK_APP=server.py && python3 -m flask run -h 0.0.0.0

# windows
# set FLASK_APP=server.py && py -m flask run -h 0.0.0.0

# CLI usage
# set FLASK_APP=server.py && py -m flask infos


from __future__ import print_function
from flask import Flask, request, url_for, send_file, render_template, Response
from datetime import datetime
from threading import Timer
import sqlite3, sys, uuid, click, urllib.request, re
from bs4 import BeautifulSoup

app = Flask(__name__)

app.debug = False



# CUSTOM VARS

PORT=5000
CERTTIMER=300



# DB

db = 'db/simotool.db'



# CLI

def print_row(col1, col2, col3):
	click.echo("%-15s %-35s %-55s" % (col1, col2, col3))

@app.cli.command('infos')
def getInfos():
		comps = getComp()
		for comp in comps:
				print_row('Computer', 'Mail', 'Last Request')
				print_row(comp[4], comp[2], comp[1])
				print_row("", "", "")
				comp_infs = getLogsById(str(comp[0]))
				i = 0
				for ci in comp_infs:
						# Temporaire si on veut limiter le nombre de résultats affiché
						if i > 9:
								break
						click.echo(ci)
						print_row("", "", "")
						i += 1
						
		return 0





# APP

@app.route('/')
def displayIndex():
	return render_template('accueil.html', titre=["Liste des ordis", "Dernières alertes du CERT"], computers=getComp(), alerts=getAlerts())



@app.route('/comp/<i>')
def singleComputerPage(i):
	return render_template('singlecomp.html', computer=getCompById(i), logs=getLogsById(i))



# add a computer in db
@app.route('/add', methods=['GET', 'POST'])
def displayFormAddComputer():
	# display only form
	if not request.form:
		return render_template('add.html')
	else:
		# display error
		if(request.form['email'] is '' or request.form['timeRequest'] is '' or request.form['timeAlert'] is ''):
			return render_template('add.html', error=True)
		# save new computer
		else:
			uuid = newComputer(request.form['email'], request.form['timeRequest'], request.form['timeAlert'])
			return render_template('add.html', uuid=uuid, frequency=request.form['timeRequest'])



# generate the file, then download it
@app.route('/sonde/<uuid>/<freq>')
def getProbe(uuid, freq):
	
	with urllib.request.urlopen('https://api.ipify.org') as content:
		ip = re.search( r'[0-9]+(?:\.[0-9]+){3}', str(content.read())).group(0)

	resp = Response(
		render_template('sonde.sh', ip=ip, port=PORT, secret=uuid, frequency=freq),
		mimetype='application/x-sh'
	)
	resp.headers.set('Content-Disposition', 'attachment', filename='sonde.sh')
	return resp



# update computer's data
#data/${SECRET}/${ho}/${GPUname}/${CPUcores}/${CPUname}
@app.route('/data/<secret>/<hostname>/<gpuname>/<cpucores>/<cpuname>')
def updateComputerInfos(secret, hostname, gpuname, cpucores, cpuname):
	updateComputer(secret, hostname, gpuname, cpucores, cpuname)
	
	return 'okay'



# add a new log from the probe
# /${SECRET}/${usedRam}/${usedCPU}/${activeUsers}/${tempSensors}/${fanSensors}/${CPUfreq}/${up}
@app.route('/data/<secret>/<ram>/<cpu>/<users>/<temp>/<fans>/<cpufreq>/<uptime>')
def handleData(secret, ram, cpu, users, temp, fans, cpufreq, uptime):

	compId = getComputerIdFromSecret(secret)	
	setLog(compId, ram, cpu, users, temp, fans, cpufreq, uptime)
	return 'okay'





# FUNCTIONS
# --sensors table

def getLogsById(i):
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("select * from sensors where id_computer = ?", i)
		rows = cur.fetchall()
		return rows

def setLog(compId, ram, cpu, users, temp, fans, cpufreq, uptime):
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("insert into sensors(id_computer, time_request, ram_usage, cpu_load, users, temperatures, fans_states, cpu_freq, uptime) values (?, ?, ?, ?, ?, ?, ?, ?, ?)",(compId, datetime.now(), ram, cpu, users, temp, fans, cpufreq, uptime))


# --computer table

def getCompById(i):
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("select * from computer where id = ?", i)
		row = cur.fetchall()
		return row[0]

def getComp():
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("select * from computer")
		rows = cur.fetchall()
		return rows

# get the is from the secret uuid
def getComputerIdFromSecret(s):
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("select id from computer where secret = ?", (s, ))
		idC = cur.fetchall()
		return idC[0][0]

def newComputer(mail, request, alert):
	with sqlite3.connect(db) as dbcon:
		sec = uuid.uuid4().hex
		cur = dbcon.cursor()
		cur.execute("insert into computer(mail, time_alert, secret, frequency) values (?, ?, ?, ?)", (mail, alert, sec, request))
		return sec

def updateComputer(secret, hostname, gpuname, cpucores, cpuname):
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("update computer set lastrequest=?, name=?, gpu_name=?, cpu_cores=?, cpu_model=? where secret=?", (datetime.now(), hostname, gpuname, cpucores, cpuname, secret))		


# --cert table

def saveLastCertAlert(soupDate, soupUrl, soupName):
	with sqlite3.connect(db) as dbcon:
		sec = uuid.uuid4().hex
		cur = dbcon.cursor()
		cur.execute("select id from cert where date_alert = ? and url = ? and title = ?", (soupDate, soupUrl, soupName))
		certAlert = cur.fetchall()

		if not certAlert:
			cur.execute("insert into cert(date_alert, url, title) values (?, ?, ?)", (soupDate, soupUrl, soupName))
			lastId = cur.lastrowid - 10

			cur.execute("delete from cert where id < ?", (lastId, ))

			app.logger.info('text: %s', lastId)

			return True
		return False

def getAlerts():
	with sqlite3.connect(db) as dbcon:
		cur = dbcon.cursor()
		cur.execute("select * from cert order by id desc")
		rows = cur.fetchall()
		return rows





# CERT ISSUES

def getCert(interval):
	Timer(interval, getCert, [interval]).start()
	link = "https://www.cert.ssi.gouv.fr/"
	f = urllib.request.urlopen(link)
	htmlcode = f.read()
	soup = BeautifulSoup(htmlcode, 'html.parser')

	# last alert
	soup = soup.find('div', {"class":"item cert-alert open"})

	# extract data
	soupDate = soup.find('span', {"class":"item-date"}).text
	soupUrl = 'https://www.cert.ssi.gouv.fr/' + soup.find('a', href=True)['href']
	soupName = soup.find('span', {"class":"item-title"}).text
	
	# save data
	saveLastCertAlert(soupDate, soupUrl, soupName)


# get new CERT alert every CERTTIMERmin (default 5)
getCert(CERTTIMER)